import { Component, OnInit } from '@angular/core';
import { TableService } from '../services/table.service';
import { Citation } from '../interfaces/models';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  columnIds: string[] = ['id', 'title', 'status', 'created', 'more'];
  private columnLabels: string[] = ['Id', 'Title', 'Status', 'Created', 'More Info'];
  dataSource: Citation[] = [];

  constructor( private tableService: TableService ) {
    tableService.getData().subscribe((r: Citation[]) => {
      console.log('r: ', r);
      this.dataSource = r;
    });
  }

  getLabel(id: string): string {
    return this.columnLabels[this.columnIds.indexOf(id)];
  }

  getMoreInfo(idNum: number): void {
  this.tableService.getDataById(idNum).subscribe((d: any) => {
    console.log('d: ', d)
  });
  }
}
