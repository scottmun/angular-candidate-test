export interface Citation {
    disseminated: string;
    distribution: string;
    id: number;
    redistributedDate: string;
}

export interface CitationDetails {
    authorAffiliations: any[]
    created: string
    disseminated: string
    distribution: string
    distributionDate: string
    downloads: any[]
    downloadsAvailable: boolean
    fundingNumbers: any[]
    id: number
    index: string
    keywords: any[]
    modified: string
    publications: any[]
    related: any[]
    sourceIdenfiers: any[]
    status: string
    stiType: string
    stiTypeDetails: string
    title: string
  }
