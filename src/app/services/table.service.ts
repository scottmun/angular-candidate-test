import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { Citation, CitationDetails } from '../interfaces/models';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor( private httpClient: HttpClient ) { }

  getData(): Observable<Citation[]> {
    return this.httpClient.post<Citation[]>('/api/citations/search', {})
    .pipe( map((res: any) => res.results.filter((item: any) => item.distribution === 'PUBLIC')));
  }

  getDataById(id: number): Observable<CitationDetails> {
    return this.httpClient.get<CitationDetails>('/api/citations/' + id)
  }
}
